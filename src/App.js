import { func } from 'prop-types';
import React, { useEffect, Fragment, useState, useRef } from 'react';
import clasess from './App.module.scss';
import Car from './Car/Car';
import Counter from './Counter/Counter'

export const ClickedContext = React.createContext(false)
function App() {

  const [cars, setCars] = useState([
    {
      name: 'Ford',
      year: 2018,
    },

    {
      name: 'Audi',
      year: 2016,
    },

    {
      name: 'Mazda',
      year: 2010,
    }

  ])

  console.log()



  const [pageTitle, setPageTitle] = useState('React Components');

  const [showCars, setShowCars] = useState(false);

  const [clicked, setClicked] = useState(false);

  const [counter, setCounter] = useState(0);

  // function changeTitleHandler(newTitle) {
  //   setPageTitle(newTitle);
  // }



  function onChangeName(name, index) {
    const car = cars[index];

    car.name = name;
    const cares = [...cars];

    cares[index] = car
    setCars(cares)
  }

  function toggleCarsHandler() {
    setShowCars((prev) => !prev);
    setCounter(prev => prev + 1);

    // textInput.current.focus();

  }

  function toggleClick() {
    setClicked((prev) => !prev)
  }



  const divStyle = {
    textAlign: 'center'
  }

  function deleteHandler(index) {
    let cares = cars.concat();
    cares.splice(index, 1)

    setCars(cares)
  }


  let carss = null;
  if (showCars) {
    carss = cars.map((car, index) => {
      return (

        <Car
          counter={counter}
          key={index}
          index={index}
          name={car.name}
          year={car.year}
          onDelete={deleteHandler.bind(this, index)}
          onChangeName={(event) => onChangeName(event.target.value, index)}
        />

      )
    })
  }

  return (

    <div className={clasess.App} style={divStyle}>
      <h1>
        {pageTitle}
      </h1>

      <ClickedContext.Provider value={clicked}>

        <Counter />

      </ClickedContext.Provider>



      <button onClick={toggleCarsHandler}>
        Toogle cars
      </button>

      <button onClick={toggleClick}>
        Change clicked
      </button>
      {/* { showCars
        ? cars.map((car, index) => {
          return (
            <Car
              key={index}
              name={car.name}
              year={car.year}
              onChangeTitle={() => changeTitleHandler(car.name)}
            />
          )
        })
        : null
      } */}
      <div style={{
        width: 400,
        margin: 'auto',
        paddingTop: '20px'
      }}>
        {carss}
      </div>

      <span>Counter: {counter}</span>
    </div>
  );
}

export default App;
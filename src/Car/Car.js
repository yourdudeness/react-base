import React, { useEffect, useRef } from 'react';
import clasess from './Car.module.scss'
import PropTypes from 'prop-types'


let Car = (props) => {


    const textInput = useRef();

    useEffect(() => {
        if (props.index === 0) {
            textInput.current.focus()
        }
    }, [])

    const classes = [
        clasess.cars,
    ];

    if (props.counter > 5) {
        classes.push(clasess.blue)
    }

    const inputClasess = [clasess.input]

    if (props.name !== '') {
        inputClasess.push(clasess.green)
    } else {
        inputClasess.push(clasess.red)
    }

    if (props.name.length > 4) {
        inputClasess.push(clasess.bold)
    }


    const style = {

        ':hover': {
            border: '1px solid #aaa',
            boxShadow: '0 4px 15px 0 rgba(0, 0, 0, .25)',
            cursor: 'pointer'
        }
    }
    return (
        <>
            <h3>
                Car name : {props.name}
            </h3>
            <p>
                Year :
            <strong>
                    {props.year}
                </strong>
            </p>
            <input
                type="text"
                ref={textInput}
                onChange={props.onChangeName}
                value={props.name}
                className={inputClasess.join(' ')}

            />



            <button onClick={props.onDelete}>
                Delete
        </button>
        </>
    )
}

Car.propTypes = {
    name: PropTypes.string.isRequired,
    year: PropTypes.number,
    onChangeName: PropTypes.func,
    onDelete: PropTypes.func
}

export default Car

// className={classes.join(' ')}
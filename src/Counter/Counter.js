import React, { useState } from 'react';
import Auxiliary from '../hoc/Auxiliary'
import Counter2 from '../Counter2/Counter2' 

const Counter = props => {
    const [count, setCount] = useState(0);

    function addCount(){
        setCount( prev => prev +1)
    }

    function downCount(){
        setCount( prev => prev - 1)
    }

    return (
        <div>
            <h1>
                Counter : {count}
            </h1>

            <Counter2 />

            <button onClick={addCount}>
                +
            </button>

            <button onClick={downCount}>
                -
            </button>
        </div>
    )

}
export default Counter